# README #


### What is this repository for? ###

* This repo is a collection of data structure implementations in C++, with ideas borrowed from different text books.
* This was used as a way to learn new data structures, and practice implementing them. 
* All the implementation code was my own, I just used the provided class header files as guides for what functions 
* should be implemented for a given data structure. Sometimes with more/less as I saw fitting.
