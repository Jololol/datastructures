#include <iostream>

#include "src/LinkedList.h"
#include "src/arrayListType.h"

void testConstructors();

int main() {

	testConstructors();

	return 0;

}


void testConstructors(){
	std::cout << "Creating linked list with data for first node and printing contents." << std::endl;
	LinkedList<int> listOne(5);
	listOne.print();

	std::cout << "Creating empty linked list and printing contents." << std::endl;
	LinkedList<int> emptyList;
	emptyList.print();

	std::cout << "Creating linked list with multiple elements and printing contents." << std::endl;
	LinkedList<int> original;
	for(int i = 0; i < 10; i++){
		original.add(i);
	}
	original.print();

	std::cout << "Creating linked list the same as previous using copy constructor and printing contents." << std::endl;
	LinkedList<int> copy(original);
	copy.print();

}