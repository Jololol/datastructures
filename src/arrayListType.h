//
// Created by jhodg on 19/07/17.
//
// An ADT to be used as a base to create a array-type classes

#ifndef DATASTRUCTURES_ARRAYLISTTYPE_H
#define DATASTRUCTURES_ARRAYLISTTYPE_H

#include <iostream>

template<class T>
class arrayListType{
public:
		/* Default constructor.
		 * Will create array of specified size, or default to 100.
		 */
		arrayListType(int size = 100){
			list = new T[size];
			maxSize = size;
			length = 0;
		}

		/* Copy constructor.
		 * Creates deep copy of the original array
		 */
		arrayListType(const arrayListType<T>& otherList){
			maxSize = otherList.maxSize;
			length = otherList.length;

			list = new T[maxSize]; // Create list of same max size of original
			for(int i = 0; i < length; i++){
				list[i] = otherList.list[i]; // Copy all elements across
			}
		}

		/* Destructor
		 * Deallocates all memory being used by list member
		 */
		~arrayListType(){
			delete list;
		}

		/* Operator Overload =
		 * Overloads the assignment operator to provide deep copy
		 */
		const arrayListType<T> operator= (const arrayListType<T> &right){
			if(this != right){
				delete list;
				maxSize = right.maxSize;
				length = right.length;

				list = new T[maxSize];
				for(int i = 0; i < length; i++){
					list[i] = right.list[i];
				}
			}
		}

		/* isEmpty()
		 * Returns a boolean true value if array is empty
		 */
		bool isEmpty() const{
			return length == 0;
		}

		/* isFull()
		 * Retunrs a boolean TRUE value is array is full
		 */
		bool isFull() const{
			return length == maxSize;
		}

		/* listSize()
		 * Returns number of elements currently stored in list
		 */
		int listSize() const{
			return length;
		}

		/* maxListSize()
		 * Returns maximum size of array
		 */
		int maxListSize() const{
			return maxSize;
		}

		/* print()
		 * Prints elements of list
		 */
		void print() const{
			for(int i = 0; i < length; i++){
				std::cout << list[i] << " ";
			}
		}

		/* isItemAtEqual()
		 * Determines whether the item at specified location is equal to the supplied item
		 * @param int location: The location in the array to compare against
		 * @param T data: The original data to compare with
		 *
		 * @return: true if the item is the same
		 */
		bool isItemAtEqual(int location, const T& data){
			return(list[location] == data);
		}




protected:
		T *list; // Array to store data
		int length; // Keeps track of length of list
		int maxSize; // Informs of maximum size of array


};

#endif //DATASTRUCTURES_ARRAYLISTTYPE_H
