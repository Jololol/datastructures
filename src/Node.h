//
// Created by jhodg on 06/07/17.
//

#ifndef DATASTRUCTURES_NODE_H
#define DATASTRUCTURES_NODE_H

template <class T>
class Node {
public:

		// Copy constructor
		Node(const Node<T> &node){
			this->data_ = node.data_;
			this->next_ = node.next_;
			this->prev_ = node.prev_;
		};

		// Construct simple node with data
		Node(T data) {
			this->data_ = data;
			this->next_ = nullptr;
			this->prev_ = nullptr;
		};

		// Construct node with pointer to next
		Node(T data, Node<T>* next) {
			this->data_ = data;
			this->next_ = next;
			this->prev_ = nullptr;
		};

		// Construct node with pointers to next and previous
		Node(T data, Node<T>* next, Node<T>* prev){
			this->data_ = data;
			this->next_ = next;
			this->prev_ = prev;

		};

		T getData() const {
			return this->data_;
		};

		Node<T>* getNext() const{
			return this->next_;
		};

		Node<T>* getPrev() const{
			return this->prev_;
		};

		void setData(const T data){
			this->data_ = data;
		};

		void setNext(Node<T>* node){
			this->next_ = node;
		};

		void setPrev(Node<T>* node){
			this->prev_ = node;
		};


private:
		T data_;
		Node<T>* next_;
		Node<T>* prev_;
};


#endif //DATASTRUCTURES_NODE_H
