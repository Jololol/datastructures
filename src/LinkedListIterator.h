//
// Created by jhodg on 06/07/17.
//

#ifndef DATASTRUCTURES_LINKEDLISTITERATOR_H
#define DATASTRUCTURES_LINKEDLISTITERATOR_H

#include "Node.h"

template <class T>
class LinkedListIterator {
public:
		LinkedListIterator(){ // Default empty constructor
			this->current_ = nullptr;
		};

		LinkedListIterator(Node<T> *ptr) { // Constructor with specifying Node
			this->current_ = ptr;
		};

		// Operator overloading functions
		T operator*(){
			return current_->getData();
		};

		LinkedListIterator<T> operator++(){
			this->current_ = current_->getNext();
			return *this;
		};

		bool operator==(const LinkedListIterator<T> &right) const{
			return (this->current_ == right.current_);
		};

		bool operator!=(const LinkedListIterator<T> &right) const{
			return (this->current_ != right.current_);
		};

private:
		Node<T>* current_;
};


#endif //DATASTRUCTURES_LINKEDLISTITERATOR_H
