//
// Created by jhodg on 01/07/17.
//

#ifndef LINKEDLISTS_LINKEDLIST_H
#define LINKEDLISTS_LINKEDLIST_H

#include <iostream>
#include "Node.h"
#include "LinkedListIterator.h"

template <class T>
class LinkedList {
public:
		LinkedList(){ // Case for creating an empty list
			head_ = nullptr;
			tail_ = nullptr;
			size_ = 0;
		};

		LinkedList(T data){ // Create a list with first item
			Node<T>* head = new Node<T>(data, nullptr, nullptr);
			tail_ = head;
			head_ = head;
			size_ = 1;
		};

		LinkedList(const LinkedList<T> &list){ // Copy constructor
			this->head_ = nullptr;
			this->tail_ = nullptr;
			this->size_ = 0;
			this->copyList(list);
		};

		~LinkedList(){ // Destructor
			this->destroyList();
		};

		// Returns true if the list is empty
		bool isEmpty() const{
			if(head_ == nullptr){
				return true;
			} else {
				return false;
			}
		};

		// Returns true if list contains item matching data
		bool contains(T data) const{
			LinkedListIterator<T> itr = this->begin();
			while(itr != this->end()){
				if(*itr == data){
					return true;
				}
				++itr;
			}
			return false;
		};

		LinkedList<T> operator=(const LinkedList<T> &right){
			if(this != &right){
				this->copyList(right);
			}

			return *this;
		};

		LinkedListIterator<T> begin(){
			LinkedListIterator<T> iterator(this->head_);
			return iterator;
		};

		LinkedListIterator<T> end(){
			LinkedListIterator<T> iterator(nullptr);
			return iterator;
		};

		LinkedListIterator<T> begin() const{
			LinkedListIterator<T> iterator(this->head_);
			return iterator;
		};

		LinkedListIterator<T> end() const{
			LinkedListIterator<T> iterator(nullptr);
			return iterator;
		};

		void add(T data){
			Node<T>* newNode = new Node<T>(data,nullptr, tail_); // Create new Node at end of list
			if(this->isEmpty()){
				head_ = newNode;
			} else {
				tail_->setNext(newNode); // Point tail to new Node
			}
			tail_ = newNode; // Set new node as tail

			size_ += 1;
		};

		void remove(T data){
			Node<T>* node = findNode(data);
			if(node == nullptr){
				std::cout << "Node containing " << data << " not found." << std::endl;
				return;
			}

			size_ -= 1;
		};

		void print(){
			if(isEmpty()){
				std::cout << "List is empty." << std::endl;
				return;
			}

			std::cout << "Linked List contains: ";

			LinkedListIterator<T> itr;
			for(itr = this->begin(); itr != this->end(); ++itr){
				std::cout << *itr << " ";
			}
			std::cout << std::endl;
		};

		int size(){
			return size_;
		};


private:

		void copyList(const LinkedList &list){
			if(this->isEmpty() == false){
				destroyList();
			}
			if(list.isEmpty()){
				this->head_ = nullptr;
				this->tail_ = nullptr;
				this->size_ = 0;
				return;
			}

			for(auto itr = list.begin(); itr != list.end(); ++itr){
				this->add(*itr);
			}
		};

		void destroyList(){
			Node<T>* temp;
			while(head_ != nullptr){
				Node<T>* temp = head_;
				head_ = head_->getNext();
				delete temp;
			}
		}; // Remove all nodes from list

		void remove(Node<T>* node){

			if(this->head_ == node){
				head_ = head_->getNext();
				head_->setPrev(nullptr);
				delete node;
				return;
			}

			Node<T>* prevNode = node->getPrev(); // Get node to be removed's previous link
			Node<T>* nextNode = node->getNext(); // Get node to be removed's next link
			prevNode->setNext(nextNode); // Link previous to nodes next
			nextNode->setPrev(prevNode); // Link next to previous node

			delete node;
		}; // Remove specified node from list

		Node<T>* findNode(T data){
			Node<T>* nodePtr = head_;
			while(nodePtr != nullptr){
				if(nodePtr->getData() == data){
					return nodePtr;
				}
				nodePtr = nodePtr->getNext();
			}
			return nullptr;
		};

		// The member variables
		int size_; // Number of items in list
		Node<T>* head_; // Points to first node in list
		Node<T>* tail_; // Points to last node in list

};


#endif //LINKEDLISTS_LINKEDLIST_H
